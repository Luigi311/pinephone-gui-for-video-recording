# Pinephone GUI for video recording

This is simple tool which allows you to record videos with Pinephone Beta (https://pine64.com/product/pinephone-beta-edition-with-convergence-package/) using GUI buttons. This tool does not provide video preview but it makes video recording so easy that even child can do it 

## What?

Just open app and choose what you want to do. "Everything" should work - if not file an issue or mr 


![Screenshot - portrait default](screenshot1.png)

![Screenshot - portrait recording](screenshot2.png)

![Screenshot - landscape recording](screenshot3.png)

![Screenshot - renaming field](screenshot4.png)


This should work with all major PP distros 

Downsides: video preview is not possible and video quality is only decent - audio quality is bad. 

Upsides: You can now record videos easily with PP 

Biggest work for this is done by:

@luigi311 - thank you so much :) <hr>
Martijn Braam   (https://blog.brixit.nl/camera-on-the-pinephone/) <hr>
Kevin Kofler (https://pine64.com/product/pinephone-beta-edition-with-convergence-package/) <hr>
Flax (https://www.youtube.com/watch?v=XvI6Qp5qXN8) <hr>
Peter (https://linmob.net/playing-with-pinephone-video-recording/) <hr>
And many more voluntary people 

## How to install?

`git clone https://gitlab.com/Alaraajavamma/pinephone-gui-for-video-recording && cd pinephone-gui-for-video-recording && sudo chmod +x install.sh && ./install.sh `

And it should just work


## License
Feel free to do what ever you want with this but no guarantees - this will probably explode your phone xD

## Something else?
If you wan't to help or find issue feel free to contact
