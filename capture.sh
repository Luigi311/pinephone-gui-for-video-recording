#!/usr/bin/env bash

help() {
  echo "usage: ./capture.sh videoname rear|front"
  exit 1
}

amixer set Capture cap
if [ -z "$1" ] ; then
  help
fi

if [ "$2" != "rear" ] && [ "$2" != "front" ] ; then
  help
fi

DIRECTION="$2"

CAMERA=""
SECONDARY_CAMERA=""
if [ "$DIRECTION" == "rear" ] ; then
  CAMERA="ov5640"
  SECONDARY_CAMERA="gc2145"
else
  CAMERA="gc2145"
  SECONDARY_CAMERA="ov5640"
fi

# Search for the camera sensor so we can define which camera to use
MAIN_SENSOR="$(media-ctl -d1 -p | awk '/: '"$CAMERA"'/ {print "\"" $4, $5 "\""}')"
SECONDARY_SENSOR="$(media-ctl -d1 -p | awk '/: '"$SECONDARY_CAMERA"'/ {print "\"" $4, $5 "\""}')"
VIDEO_SOURCE="$(media-ctl -d1 -p | awk '/\/dev\/video/ {print $NF}')"

ROTATE=0
CUR_X=$(cat /sys/bus/iio/devices/iio\:device*/in_accel_x_raw)
CUR_Y=$(cat /sys/bus/iio/devices/iio\:device*/in_accel_y_raw)
ABS_CUR_X=${CUR_X#-}
ABS_CUR_Y=${CUR_Y#-}
if [ $ABS_CUR_X -ge $ABS_CUR_Y ] ; then
  if [ $CUR_X -ge 0 ] ; then
    echo "portrait"

    # Orientation is reversed for the front camera
    if [ "$DIRECTION" == "rear" ] ; then
      ROTATE=270
    else
      ROTATE=90
    fi
  else
    echo "portrait inverted"
    
    # Orientation is reversed for the front camera
    if [ "$DIRECTION" == "rear" ] ; then
      ROTATE=90
    else
      ROTATE=270
    fi
  fi
else
  if [ $CUR_Y -lt 0 ] ; then
    echo "landscape clockwise"
    ROTATE=0
  else
    echo "landscape counterclockwise"
    ROTATE=180
  fi
fi
 
# Setup the camera sensor
media-ctl -d1 -l"${SECONDARY_SENSOR}:0->1:0[0],${MAIN_SENSOR}:0->1:0[1]"
media-ctl -d1 -V"${MAIN_SENSOR}:0[fmt:UYVY8_2X8/1280x720@1/30]" || exit $?

ffmpeg -input_format yuv420p -s 1280x720 -f video4linux2 -thread_queue_size 4096 -i "${VIDEO_SOURCE}" -f pulse -thread_queue_size 256 -i alsa_input.platform-sound.HiFi__hw_PinePhone_0__source -c:a aac -c:v libx264 -preset ultrafast -qp 23 "${HOME}/Videos/.temp.${1}.mp4"
ffmpeg -i "${HOME}/Videos/.temp.${1}.mp4" -map_metadata 0 -metadata:s:v rotate="${ROTATE}" -codec copy "${HOME}/Videos/${1}.mp4"
rm -f "${HOME}/Videos/.temp.${1}.mp4"
