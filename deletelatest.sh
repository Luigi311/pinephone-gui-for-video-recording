#!/usr/bin/env bash

# Specify the folder path where the .mp4 files are located
folder="${HOME}/Videos/"

# Find the latest .mp4 file in the folder
latest_file=$(ls -t "$folder"/*.mp4 2>/dev/null | head -1)

# Check if any .mp4 files were found
if [ -z "$latest_file" ]; then
  echo "No .mp4 files found in the folder."
  exit 1
fi

input=$(yad --title="VideoRec Delete" \
	--image=/opt/VideoRec/delete.svg \
        --text "Do you want to delete $latest_file" \
        --buttons-layout=center \
        --button=Cancel:1 \
        --button=Delete:0)

case $? in
1) echo aborting; exit 1;;
0) cd $folder && rm $latest_file; exit 1;;
esac

