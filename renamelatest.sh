#!/usr/bin/env bash

# Specify the folder path where the .mp4 files are located
folder="${HOME}/Videos/"

# Find the latest .mp4 file in the folder
latest_file=$(ls -t "$folder"/*.mp4 2>/dev/null | head -1)

# Check if any .mp4 files were found
if [ -z "$latest_file" ]; then
  echo "No .mp4 files found in the folder."
  exit 1
fi

input=$(yad --title="VideoRec Rename" \
	--image=/opt/VideoRec/rename.svg \
        --entry \
        --text "Enter new name for latest video file" \
        --buttons-layout=center \
        --button=Rename:0)

printf 'INPUT: %s\n' "$input" && cd $folder && mv "$latest_file" "$input".mp4

