#!/usr/bin/env bash

help() {
    echo "usage: ./record.sh rear|front"
    exit 1
}

if [ -z "$1" ]; then
    help
fi

if [ "$1" != "rear" ] && [ "$1" != "front" ]; then
    help
fi

TIMENOW=$(date +%A-%d-%m-%Y_%H_%M_%S)
DIRECTION="$1"

yad --title="VideoRec" \
    --image=/opt/VideoRec/tuxcam.svg \
   	--text="Recording ${DIRECTION} started" \
    --form \
    --field="Stop and save":fbtn "bash -c 'pkill -f ffmpeg & echo 0 > /sys/class/leds/white:flash/brightness & echo 0 > /sys/class/leds/red\:indicator/brightness'" \
    --field="Start torch":fbtn "bash -c 'echo 1 > /sys/class/leds/white:flash/brightness'" \
    --field="Stop torch":fbtn "bash -c 'echo 0 > /sys/class/leds/white:flash/brightness'" \
    --no-buttons \
    & sleep 2 && sh /opt/VideoRec/capture.sh "${TIMENOW}" "${DIRECTION}" &
    sleep 2 && echo 1 > /sys/class/leds/red\:indicator/brightness & echo 0 > /sys/class/leds/blue\:indicator/brightness & echo 0 > /sys/class/leds/green\:indicator/brightness
