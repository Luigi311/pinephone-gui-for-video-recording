#!/usr/bin/env bash

yad --title="VideoRec" \
    --image=/opt/VideoRec/tuxcam.svg \
    --text="Press record direction to begin recording based on your current orientation" \
    --form \
    --field="Record rear":fbtn "bash -c '/opt/VideoRec/record.sh rear'" \
    --field="Record front":fbtn "bash -c '/opt/VideoRec/record.sh front'" \
    --field="Open Videos folder":fbtn "bash -c 'xdg-open ${HOME}/Videos/'" \
    --field="Open latest video file":fbtn "bash -c 'cd /opt/VideoRec/ && ./latest.sh'" \
    --field="Delete latest video file":fbtn "bash -c 'cd /opt/VideoRec/ && ./deletelatest.sh'" \
    --field="Rename latest video file":fbtn "bash -c 'cd /opt/VideoRec/ && ./renamelatest.sh'" \
    --buttons-layout=center \
    --button="Quit"\
